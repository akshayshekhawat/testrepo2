public class controllingInfo {
    
    public Map<String, List<String>> dependentPickListValues {get;set;}
    public String selectedCountry {get;set;}
    public String selectedState {get;set;}
    public List<SelectOption> statesList {get;set;}
    public List<SelectOption> countryList {get;set;}
    
    public controllingInfo(){
        try{
            countryList = new List<SelectOption>();
            statesList = new List<SelectOption>();
            dependentPickListValues = new Map<String, List<String>>();
            dependentPickListValues = getFieldDependencies('User', 'Countrycode', 'statecode');
            System.debug('dependentPickListValues:: '+ dependentPickListValues.keySet());
        }
        catch(Exception ex){
            System.debug('Error:: '+ ex.getMessage() +'---'+ ex.getLineNumber() +'---'+ ex.getStackTraceString());
        }
    }
    
    private class MyPickListInfo{
        public String validFor;
    }
    
    private Map<String, List<String>> getFieldDependencies(String objectName, String controllingField, String dependentField){
        
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        
        Schema.DescribeSObjectResult describeResult = objType.getDescribe();
        Schema.DescribeFieldResult controllingFieldInfo = describeResult.fields.getMap().get(controllingField).getDescribe();
        Schema.DescribeFieldResult dependentFieldInfo = describeResult.fields.getMap().get(dependentField).getDescribe();
        
        List<Schema.PicklistEntry> controllingValues = controllingFieldInfo.getPicklistValues();
        List<Schema.PicklistEntry> dependentValues = dependentFieldInfo.getPicklistValues();
        
        for(Schema.PicklistEntry currControllingValue : controllingValues){
            System.debug('ControllingField: Label:' + currControllingValue.getLabel());
            dependentPickListValues.put(currControllingValue.getLabel(), new List<String>());
            countryList.add(new SelectOption(currControllingValue.getLabel(),currControllingValue.getLabel()));
        }
        
        for(Schema.PicklistEntry currDependentValue : dependentValues){
            String jsonString = JSON.serialize(currDependentValue);
            
            MyPickListInfo info = (MyPickListInfo) JSON.deserialize(jsonString, MyPickListInfo.class);
            
            String hexString = EncodingUtil.convertToHex(EncodingUtil.base64Decode(info.validFor)).toUpperCase();
            
            System.debug('DependentField: Label:' + currDependentValue.getLabel() + ' ValidForInHex:' + hexString + ' JsonString:' + jsonString);
            
            Integer baseCount = 0;
            
            for(Integer curr : hexString.getChars()){
                Integer val = 0;
                
                if(curr >= 65){
                    val = curr - 65 + 10;
                }
                else{
                    val = curr - 48;
                }
                
                if((val & 8) == 8){
                    System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 0].getLabel());
                    dependentPickListValues.get(controllingValues[baseCount + 0].getLabel()).add(currDependentValue.getLabel());
                }
                if((val & 4) == 4){
                    System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 1].getLabel());
                    dependentPickListValues.get(controllingValues[baseCount + 1].getLabel()).add(currDependentValue.getLabel());                    
                }
                if((val & 2) == 2){
                    System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 2].getLabel());
                    dependentPickListValues.get(controllingValues[baseCount + 2].getLabel()).add(currDependentValue.getLabel());                    
                }
                if((val & 1) == 1){
                    System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 3].getLabel());
                    dependentPickListValues.get(controllingValues[baseCount + 3].getLabel()).add(currDependentValue.getLabel());                    
                }
                
                baseCount += 4;
            }            
        } 
        System.debug('countryList:: '+ countryList);
        return dependentPickListValues;
    }
    
    public void findStates(){
        
        System.debug('selectedCountry:: ' + selectedCountry);
        
        if(selectedCountry != null){
            statesList.clear();
            for(String states : dependentPickListValues.get(selectedCountry)){
                statesList.add(new SelectOption(states,states));
            }
        }
        System.debug('Statest:: '+ statesList);
    }
}